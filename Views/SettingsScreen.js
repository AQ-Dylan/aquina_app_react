import React from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import PDFViewer from "../Components/PDFViewer";
import AquinaLayout from "../Config/style";
import AsyncStorage from "@react-native-async-storage/async-storage";

import LoadingIndicator from "../Components/LoadingIndicator";

class SettingsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: false, userLoaded: false };
    this.organizationName;
    this.firstName;
    this.lastName;
    this._getUserData();
  }

  _getUserData = () => {
    try {
      AsyncStorage.getItem("userInfo").then((user) => {
        var userJson = JSON.parse(user);
        //console.log(userJson);
        this.token = userJson.Result.Token;
        this.organizationId = userJson.Result.OrganizationId;
        this.organizationName = userJson.Result.OrganizationName;
        this.firstName = userJson.Result.FirstName;
        this.lastName = userJson.Result.LastName;

        this.setState({ isLoading: false });
      });
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    if (!this.state.isLoading) {
      return (
        <SafeAreaView style={AquinaLayout.container}>
          {/* <PDFViewer /> */}
          <View style={{ alignItems: "center" }}>
            <Text>{this.organizationName}</Text>
          </View>
        </SafeAreaView>
      );
    } else {
      return <LoadingIndicator />;
    }
  }
}

export default SettingsScreen;
