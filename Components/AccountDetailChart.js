import React from "react";
import { View, Text } from "react-native";
import { ProgressCircle } from "react-native-svg-charts";
import { Card } from "react-native-elements";
import NumberFormat from "react-number-format";
import AquinaLayout from "../Config/style";

class AccountDetailChart extends React.Component {
  constructor(props) {
    super(props);
    //this.lineUsed = 0.8;
    this.lineUsed = this.props.currentBalance / this.props.underwrittenAmount;
    this.color =
      this.lineUsed <= 0.3
        ? "#ee2400"
        : this.lineUsed <= 0.5 && this.lineUsed > 0.3
        ? "#eec600"
        : this.lineUsed <= 0.75 && this.lineUsed > 0.5
        ? "#eeee00"
        : this.lineUsed <= 1 && this.lineUsed > 0.75
        ? "#00ee53"
        : AquinaLayout.primaryColor;
  }
  render() {
    return (
      <Card>
        <Card.Title>
          MaxLine:
          <NumberFormat
            value={this.props.underwrittenAmount}
            displayType={"text"}
            thousandSeparator={true}
            prefix={"$"}
            renderText={(value) => (
              <Text style={{ fontSize: 18, fontWeight: "bold" }}>{value}</Text>
            )}
          />
        </Card.Title>
        <View style={{ height: 120 }}>
          <ProgressCircle
            startAngle={8}
            endAngle={4.6}
            strokeWidth={12}
            style={{ height: 200 }}
            progress={this.lineUsed}
            progressColor={this.color}
          />
        </View>
        <View
          style={{
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            bottom: -70,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Text>
            <NumberFormat
              value={this.props.currentBalance}
              displayType={"text"}
              thousandSeparator={true}
              prefix={"$"}
              renderText={(value) => (
                <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                  {value}
                </Text>
              )}
            />
          </Text>
        </View>
      </Card>
    );
  }
}

export default AccountDetailChart;
