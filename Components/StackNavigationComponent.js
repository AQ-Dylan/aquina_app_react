import React from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  TextInput,
  TouchableOpacity,
  Button,
} from "react-native";
import { getFocusedRouteNameFromRoute } from "@react-navigation/native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { createStackNavigator } from "@react-navigation/stack";
import NavigationComponent from "./NavigationComponent";
import AquinaLayout from "../Config/style";
import AsyncStorage from "@react-native-async-storage/async-storage";

import DocumentViewerScreen from "../Views/DocumentViewerScreen";

const Stack = createStackNavigator();

function getHeaderTitle(route) {
  //console.log(route);
  const routeName = getFocusedRouteNameFromRoute(route) ?? "Default";
  console.log(routeName);

  switch (routeName) {
    case "Dashboard":
      return "Dashboard";
    case "Settings":
      return "Settings";
    case "Board":
      return "Board";
    case "Default":
      return "Dashboard";
  }
}

class StackNavigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: false };
  }

  render() {
    return (
      <Stack.Navigator>
        <Stack.Screen
          name="Aquina"
          component={NavigationComponent}
          options={({ route }) => ({
            // headerShown: false,
            headerTitle: getHeaderTitle(route),
            headerStyle: {
              backgroundColor: AquinaLayout.primaryColor,
            },
            headerRight: () => (
              <MaterialCommunityIcons
                style={{ padding: 10 }}
                name="logout-variant"
                size={25}
                onPress={() => this.props.onLogoutAttempt()}
              />
            ),
            headerLeft: () => (
              <MaterialCommunityIcons
                style={{ padding: 10 }}
                name="reorder-horizontal"
                size={25}
                onPress={() => alert("This is a button!")}
              />
            ),
          })}
        ></Stack.Screen>
        <Stack.Screen
          name="DocumentViewer"
          component={DocumentViewerScreen}
          options={() => ({
            //headerShown: false,
          })}
        ></Stack.Screen>
      </Stack.Navigator>
    );
  }
}

export default StackNavigation;
