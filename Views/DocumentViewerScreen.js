import React from "react";
import { SafeAreaView } from "react-native";
import PDFViewer from "../Components/PDFViewer";
import AquinaLayout from "../Config/style";
import AsyncStorage from "@react-native-async-storage/async-storage";

import LoadingIndicator from "../Components/LoadingIndicator";

class DocumentViewerScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: false };
    //console.log(this.props.route.params);
    //this._getUserData();
  }

  // _getUserData = () => {
  //   try {
  //     AsyncStorage.getItem("userInfo").then((user) => {
  //       var userJson = JSON.parse(user);
  //       this.token = userJson.Result.Token;
  //       this.organizationId = userJson.Result.OrganizationId;
  //       this.organizationName = userJson.Result.OrganizationName;
  //       this.firstName = userJson.Result.FirstName;
  //       this.lastName = userJson.Result.LastName;

  //       this.setState({
  //         isLoading: false,
  //       });
  //     });
  //   } catch (e) {
  //     console.log(e);
  //   }
  // };

  render() {
    if (!this.state.isLoading) {
      return (
        <SafeAreaView style={AquinaLayout.container}>
          <PDFViewer
            organizationId={this.props.route.params.organizationId}
            docuemntId={this.props.route.params.documentId}
          />
          {/* <View style={{ alignItems: "center" }}>
            <Text>{this.organizationName}</Text>
          </View> */}
        </SafeAreaView>
      );
    } else {
      return <LoadingIndicator />;
    }
  }
}

export default DocumentViewerScreen;
