import React from "react";
import { Text, View, TouchableOpacity, ActivityIndicator } from "react-native";
import { Button, Divider } from "react-native-elements";
import AquinaLayout from "../Config/style";

class ConfirmModalContent extends React.Component {
  constructor(props) {
    super(props);
    //console.log(props);
  }

  render() {
    return (
      <View>
        <View
          style={{
            width: 350,
            height: 300,
            borderRadius: 10,
            backgroundColor: "white",
          }}
        >
          <View
            style={{
              padding: 10,
              alignItems: "center",
            }}
          >
            <Text style={{ fontSize: 30 }}>{this.props.header}</Text>
          </View>
          <Divider style={{ backgroundColor: "black" }}></Divider>
          <View
            style={{
              flex: 1,
              padding: 5,
              justifyContent: "center",
              alignContent: "stretch",
              alignItems: "center",
            }}
          >
            <Text>{this.props.content}</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <View style={{ flex: 1 }}>
              <Button
                buttonStyle={{ height: 50 }}
                type="clear"
                title="Cancel"
                onPress={this.props.cancelCallback}
              />
            </View>
            <View style={{ flex: 1 }}>
              <Button
                buttonStyle={{ height: 50 }}
                title="Confirm"
                onPress={this.props.confirmCallback}
              />
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default ConfirmModalContent;
