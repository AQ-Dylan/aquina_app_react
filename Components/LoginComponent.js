import React from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import AquinaLayout from "../Config/style";

class LoginComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: false };
  }

  render() {
    return (
      <>
        <Image
          style={{
            top: -100,
            resizeMode: "contain",
            width: 200,
          }}
          source={require("../assets/Aquina_FullColor_Tagline.png")}
        ></Image>
        <View style={styles.logInBox}>
          <View style={{ paddingBottom: 10 }}>
            <TextInput
              style={styles.input}
              placeholder="Email"
              inlineImageLeft="username"
            ></TextInput>
          </View>
          <View style={{ paddingBottom: 20 }}>
            <TextInput
              style={styles.input}
              secureTextEntry={true}
              placeholder="Password"
            ></TextInput>
          </View>

          <View style={{ paddingBottom: 10 }}>
            <TouchableOpacity
              style={AquinaLayout.primaryButton}
              onPress={() => {
                this.setState({ isLoading: true });
                this.props.onLoginAttempt(
                  "aesthetiquemedical@gmail.com",
                  "Password@202"
                );
              }}
              disabled={this.state.isLoading}
            >
              {!this.state.isLoading ? (
                <Text>Log In</Text>
              ) : (
                <ActivityIndicator color={"black"} />
              )}
            </TouchableOpacity>
          </View>
        </View>
      </>
    );
  }
}

export default LoginComponent;

const styles = StyleSheet.create({
  logInBox: {
    width: "90%",
    top: -200,
    justifyContent: "space-around",
  },
  input: {
    height: 40,
    borderWidth: 0.5,
    borderColor: "grey",
    padding: 5,
  },
  primaryButton: {
    alignItems: "center",
    backgroundColor: AquinaLayout.primaryColor,
    padding: 10,
  },
  secondaryButton: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 10,
  },
});
