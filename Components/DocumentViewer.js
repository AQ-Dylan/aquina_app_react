import React from "react";
import { View, Text, Button, ActivityIndicator } from "react-native";
import AquinaApi from "../Services/AquinaApi";
import { Card } from "react-native-elements";
import NumberFormat from "react-number-format";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import AquinaLayout from "../Config/style";
import { TouchableOpacity } from "react-native-gesture-handler";

class DocumentViewer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: true };
    this._LoadDocuments();
    this.documents = [];
  }

  _LoadDocuments = () => {
    AquinaApi._RecentDocuments(
      this.props.OrganizationId,
      this.props.Token
    ).then((data) => {
      this.documents = data.Result;
      this.setState({ isLoading: false });
    });
  };

  render() {
    if (this.state.isLoading) {
      return (
        <View>
          <Card>
            <Card.Title>
              <Text style={{ fontSize: 20 }}>Recent Documents</Text>
            </Card.Title>
            <Card.Divider />
            <View>
              <ActivityIndicator />
            </View>
          </Card>
        </View>
      );
    } else {
      return (
        <View>
          <Card>
            <Card.Title>
              <Text style={{ fontSize: 20 }}>Recent Documents</Text>
            </Card.Title>
            <Card.Divider />
            <View>
              {this.documents.map((item, i) => {
                return (
                  <View
                    key={i}
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-evenly",
                    }}
                  >
                    <View>
                      <Text>{item.FileName}</Text>
                    </View>
                    <View>
                      <NumberFormat
                        value={item.RequestedAmount}
                        displayType={"text"}
                        thousandSeparator={true}
                        prefix={"$"}
                        renderText={(value) => (
                          <Text style={{ fontSize: 15, fontWeight: "bold" }}>
                            {value}
                          </Text>
                        )}
                      />
                    </View>
                    <View>
                      <Text>{item.Status}</Text>
                    </View>
                    {item.Status == "Funded" ? (
                      <TouchableOpacity
                        onPress={() => {
                          this.props.ViewDocumentCallBack(item.Id);
                        }}
                        disabled={this.state.isLoading}
                      >
                        <MaterialCommunityIcons name="download" size={25} />
                      </TouchableOpacity>
                    ) : null}
                  </View>
                );
              })}
            </View>
          </Card>
        </View>
      );
    }
  }
}

export default DocumentViewer;
