import React from "react";
import { SafeAreaView, View, ScrollView } from "react-native";
import AquinaLayout from "../Config/style";
import AsyncStorage from "@react-native-async-storage/async-storage";

import LoadingIndicator from "../Components/LoadingIndicator";
import PaymentCard from "../Components/PaymentCard";
import FundingCard from "../Components/FundingCard";
import ChartTest from "../Components/ChartTest";
import DocumentViewer from "../Components/DocumentViewer";
import AccountDetailChart from "../Components/AccountDetailChart";
import { Enviorment } from "@env";

import AquinaApi from "../Services/AquinaApi";

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this._getUserData();
    this.state = {
      currentProduct: 0,
      isLoading: true,
      userLoaded: false,
    };
    this.token;
    this.organizationName;
    this.organizationId;
    this.firstName;
    this.lastName;
    this.UnderwrittenAmount = 0;
    this.PayOffAmount = 0.0;
    this.PayOffSavingAmount = 0.0;
    this.CurrentBalance = 0.0;
  }

  _onDownloadDocument = (id) => {
    console.log(this.id, this.organizationId);
    this.props.navigation.navigate("DocumentViewer", {
      organizationId: this.organizationId,
      documentId: id,
    });
  };

  _getUserData = () => {
    try {
      AsyncStorage.getItem("userInfo").then((user) => {
        var userJson = JSON.parse(user);
        //console.log(userJson);
        this.token = userJson.Result.Token;
        this.organizationId = userJson.Result.OrganizationId;
        this.organizationName = userJson.Result.OrganizationName;
        this.firstName = userJson.Result.FirstName;
        this.lastName = userJson.Result.LastName;
        AquinaApi._GetAvailableFunds(
          userJson.Result.OrganizationId,
          userJson.Result.Token
        ).then((response) => {
          this.PayOffAmount =
            response.Result.PayOffAmount > 1 ? response.Result.PayOffAmount : 0;
          this.PayOffSavingAmount = response.Result.PayOffSavingAmount;
          this.UnderwrittenAmount = response.Result.UnderwrittenAmount;
          this.CurrentBalance = response.Result.CurrentBalance;
          this.setState({
            userLoaded: true,
            currentProduct: userJson.Result.Products[0],
            isLoading: false,
          });
        });
      });
    } catch (e) {
      console.log(e);
    }
  };

  _getAvailableFunds = () => {
    try {
      console.log("Refresh Data");
      this.setState({
        isLoading: true,
      });
      AquinaApi._GetAvailableFunds(this.organizationId, this.token).then(
        (response) => {
          //console.log(response);
          this.PayOffAmount =
            response.Result.PayOffAmount > 1 ? response.Result.PayOffAmount : 0;
          this.PayOffSavingAmount = response.Result.PayOffSavingAmount;
          this.CurrentBalance = response.Result.CurrentBalance;
          this.setState({
            isLoading: false,
          });
        }
      );
    } catch (e) {
      console.log(e);
    }
  };

  handleChange = (amount) => {
    this.setState({ PaymentAmount: amount });
  };

  render() {
    if (!this.state.isLoading) {
      return (
        <SafeAreaView style={AquinaLayout.container}>
          {/* <NavigationHeader /> */}
          <ScrollView
          // onScroll={(e) => {
          //   scrollY.setValue(e.nativeEvent.contentOffset.y);
          // }}
          >
            <AccountDetailChart
              underwrittenAmount={this.UnderwrittenAmount}
              currentBalance={this.PayOffAmount}
            />
            <PaymentCard
              UnderwrittenAmount={this.UnderwrittenAmount}
              AvailableFundsCallback={this._getAvailableFunds}
              PayOffAmount={this.PayOffAmount.toFixed(2)}
              PayOffSavingAmount={this.PayOffSavingAmount.toFixed(2)}
              OrganizationId={this.organizationId}
              Token={this.token}
            />
            <FundingCard
              AvailableFundsCallback={this._getAvailableFunds}
              UnderwrittenAmount={this.UnderwrittenAmount}
              CurrentBalance={this.CurrentBalance}
            />
            <DocumentViewer
              ViewDocumentCallBack={this._onDownloadDocument}
              OrganizationId={this.organizationId}
              Token={this.token}
            />
            <View>
              <ChartTest />
            </View>
            {/* <View>
              <ChartTest />
            </View>
            <View>
              <ChartTest />
            </View> */}
          </ScrollView>
        </SafeAreaView>
      );
    } else {
      return <LoadingIndicator />;
    }
  }
}

export default HomeScreen;
