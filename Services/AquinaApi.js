class AquinaApi {
  constructor() {
    this.baseUrl = "https://dev-api.aquinahealth.com/api/v1";
    this.headers = {
      Authorization: "",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    };
  }

  async _SignInUser(payload) {
    //console.log(this.baseUrl);
    //console.log(JSON.stringify(payload));
    let response = await fetch(`${this.baseUrl}/aquina/user`, {
      method: "POST",
      headers: this.headers,
      body: JSON.stringify(payload),
    });
    var data = await response.json();
    if (data.isError) {
      console.log(data.Message);
    } else {
      return data;
    }
  }

  async _GetCurrentBalance(organizationId, token) {
    this.headers.Authorization = token;
    let response = await fetch(
      `${this.baseUrl}/praxis/currentBalance?organization=${organizationId}`,
      {
        method: "GET",
        headers: this.headers,
      }
    );
    var data = await response.json();
    //console.log(data);
    if (data.isError) {
      console.log(data.Message);
    } else {
      return data;
    }
  }

  async _GetAvailableFunds(organizationId, token) {
    this.headers.Authorization = token;
    let response = await fetch(
      `${this.baseUrl}/praxis/availableFunds?organization=${organizationId}`,
      {
        method: "GET",
        headers: this.headers,
      }
    );
    var data = await response.json();
    //console.log(data);
    if (data.isError) {
      console.log(data.Message);
    } else {
      return data;
    }
  }

  async _ProcessPaymant(organizationId, token, payload) {
    this.headers.Authorization = token;
    let response = await fetch(
      `${this.baseUrl}/praxis/payment?organization=${organizationId}`,
      {
        method: "POST",
        headers: this.headers,
        body: JSON.stringify(payload),
      }
    );
    var data = await response.json();
    if (data.isError) {
      console.log(data.Message);
    } else {
      return data;
    }
  }

  async _RecentDocuments(organizationId, token) {
    this.headers.Authorization = token;
    let response = await fetch(
      `${this.baseUrl}/praxis/requestFunds?organization=${organizationId}`,
      {
        method: "GET",
        headers: this.headers,
      }
    );
    var data = await response.json();
    //console.log(data);
    if (data.isError) {
      console.log(data.Message);
    } else {
      return data;
    }
  }
}

const aquinaApi = new AquinaApi();
export default aquinaApi;
