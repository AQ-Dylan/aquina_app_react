import React from "react";
import { SafeAreaView, View, ActivityIndicator } from "react-native";
import AquinaLayout from "../Config/style";

export default class LoadingIndicator extends React.Component {
  render() {
    return (
      <SafeAreaView style={AquinaLayout.container}>
        <View>
          <ActivityIndicator />
        </View>
      </SafeAreaView>
    );
  }
}
