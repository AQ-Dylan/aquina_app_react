import React from "react";
import { View } from "react-native";
import { WebView } from "react-native-webview";

export default class PDFViewer extends React.Component {
  constructor(props) {
    super(props);
    //console.log(this.props);
  }
  render() {
    const baseUrl = "https://dev-api.aquinahealth.com/api/v1";
    const Url = `${baseUrl}/docusign/document?organizationId=${this.props.organizationId}&documentId=${this.props.docuemntId}`;
    //console.log(Url);
    return (
      <WebView
        source={{
          uri: Url,
        }}
      ></WebView>
    );
  }
}
