import React from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  Animated,
  TouchableOpacity,
  Header,
} from "react-native";
import AquinaLayout from "../Config/style";

class NavigationHeader extends React.Component {
  render() {
    return (
      <Header
        centerComponent={{
          text: "Aquina",
          style: { color: AquinaLayout.primaryColor },
        }}
      />
    );
  }
}

export default NavigationHeader;
