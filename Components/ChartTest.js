import React from "react";
import { View, Text } from "react-native";
import {
  LineChart,
  ProgressCircle,
  YAxis,
  Grid,
} from "react-native-svg-charts";
import { Card } from "react-native-elements";

import AquinaLayout from "../Config/style";

class ChartTest extends React.Component {
  render() {
    const data = [
      50,
      10,
      40,
      95,
      -4,
      -24,
      85,
      91,
      35,
      53,
      -53,
      24,
      50,
      -20,
      -80,
    ];

    const contentInset = { top: 20, bottom: 20 };

    return (
      <Card>
        <Card.Title>Example</Card.Title>
        {/* <View style={{ height: 100 }}>
          <ProgressCircle
            style={{ height: 100 }}
            progress={0.7}
            progressColor={"rgb(134, 65, 244)"}
            //children={<Text style={{ fontSize: 30 }}>smdbjb</Text>}
          />
        </View> */}
        <View style={{ height: 200, flexDirection: "row" }}>
          <YAxis
            data={data}
            contentInset={contentInset}
            svg={{
              fill: "grey",
              fontSize: 10,
            }}
            numberOfTicks={10}
            formatLabel={(value) => `${value}ºC`}
          />

          <LineChart
            style={{ flex: 1, marginLeft: 16 }}
            data={data}
            svg={{ stroke: "rgb(134, 65, 244)" }}
            contentInset={contentInset}
          >
            <Grid />
          </LineChart>
        </View>
      </Card>
    );
  }
}

export default ChartTest;
