import React from "react";
import { StatusBar } from "expo-status-bar";
import { StyleSheet, SafeAreaView } from "react-native";
import AquinaLayout from "./Config/style";
import LoginComponent from "./Components/LoginComponent";
import AquinaApi from "./Services/AquinaApi";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { NavigationContainer } from "@react-navigation/native";
import StackNavigation from "./Components/StackNavigationComponent";
import NavigationComponent from "./Components/NavigationComponent";
import jwt_decode from "jwt-decode";

class App extends React.Component {
  constructor(props) {
    super(props);
    this._userValid();
    this.state = { isLoading: true, userLoaded: false, currentProduct: 0 };
  }

  _userValid = () => {
    try {
      AsyncStorage.getItem("userInfo")
        .then((user) => {
          var userJson = JSON.parse(user);
          var token = jwt_decode(userJson.Result.Token);
          if (Date.now() >= token.exp * 1000) {
            console.log("Not Valid");
            this.setState({ isLoading: false });
          } else {
            console.log("Valid");
            this.setState({ userLoaded: true, isLoading: false });
          }
        })
        .catch((e) => {
          console.log(e);
        });
    } catch (e) {
      console.log(e);
    }
  };

  _onLoginAttempt = async (email, password) => {
    let payload = await AquinaApi._SignInUser({
      Email: email,
      Password: password,
    });
    try {
      await AsyncStorage.setItem("userInfo", JSON.stringify(payload));
    } catch (e) {
      console.log(e);
    }
    this.setState({
      userLoaded: true,
      currentProduct: payload.Result.Products[0],
    });
  };

  _onLogoutAttempt = async (email, password) => {
    AsyncStorage.clear();
    console.log("Logout");
    this.setState({
      userLoaded: false,
      currentProduct: null,
    });
  };

  render() {
    if (this.state.userLoaded) {
      return (
        <>
          <StatusBar barStyle="dark-content" />
          <NavigationContainer>
            <StackNavigation onLogoutAttempt={this._onLogoutAttempt} />
          </NavigationContainer>
        </>
      );
    } else {
      return (
        <>
          <StatusBar barStyle="dark-content" />
          <SafeAreaView style={AquinaLayout.logo_container}>
            <LoginComponent onLoginAttempt={this._onLoginAttempt} />
          </SafeAreaView>
        </>
      );
    }
  }
}

export default App;

const styles = StyleSheet.create({
  logInBox: {
    position: "absolute",
    width: "90%",
    justifyContent: "space-around",
  },
  input: {
    height: 40,
    borderWidth: 0.5,
    borderColor: "grey",
  },
  primaryButton: {
    alignItems: "center",
    backgroundColor: AquinaLayout.primaryColor,
    padding: 10,
  },
  secondaryButton: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 10,
  },
});
