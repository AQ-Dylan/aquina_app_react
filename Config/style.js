import { ColorPropType } from "react-native";

export default {
  primaryColor: "#00adee",
  secondaryColor: "",
  black: "",
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#ffffff",
    justifyContent: "center",
  },
  logo_container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#ffffff",
    alignItems: "center",
    justifyContent: "center",
  },
  primaryInput: {
    padding: 5,
    height: 40,
    borderWidth: 0.5,
    borderColor: "grey",
  },
  primaryButton: {
    alignItems: "center",
    backgroundColor: "#00adee",
    padding: 15,
  },
};
