import React from "react";
import { Text, View, TouchableOpacity, ActivityIndicator } from "react-native";
import Modal from "react-native-modal";
import { Card } from "react-native-elements";
import AquinaLayout from "../Config/style";
import NumberFormat from "react-number-format";
import ConfirmModalContent from "./ConfirmModalContent";
import CurrencyInput from "react-native-currency-input";

class FundingCard extends React.Component {
  constructor(props) {
    super(props);
    //console.log(props);
    this.state = {
      isLoading: false,
      InputFundAmount: null,
      isModalVisible: false,
      isError: false,
      isSuccess: false,
      Message: null,
    };
    if (this.props.UnderwrittenAmount * 0.1 <= 5000) {
      this.minPayment = parseFloat(
        (this.props.UnderwrittenAmount * 0.1).toFixed(2)
      );
    } else {
      this.minPayment = 5000;
    }
    this.remainingFunding =
      this.props.UnderwrittenAmount - this.props.CurrentBalance;
  }

  _CancelFunding = async () => {
    this.setState({ isModalVisible: false, InputFundAmount: null });
  };

  _RequestFunding = async () => {
    this.setState({ isLoading: true });

    var FundAmount = this.state.InputFundAmount
      ? parseFloat(
          this.state.InputFundAmount.replace("$", "")
            .replace(" ", "")
            .replace(",", "")
        )
      : 0;

    //console.log(FundAmount);
    if (FundAmount >= this.minPayment && FundAmount <= this.remainingFunding) {
      this.setState({
        isModalVisible: false,
        isError: false,
        isSuccess: true,
        Message: "Request was successful, please check back later.",
      });
      setTimeout(() => {
        this.setState({
          isLoading: false,
          isError: false,
          isSuccess: false,
          Message: null,
        });
        this.props.AvailableFundsCallback();
      }, 3000);
    } else {
      this.setState({
        isModalVisible: false,
        isError: true,
        isSuccess: false,
        isLoading: false,
        Message:
          FundAmount < this.minPayment
            ? `Minimum request is $${this.minPayment}`
            : FundAmount > this.remainingFunding
            ? `Can't exceed remaining balance of $${this.remainingFunding}`
            : "Uknown error",
      });
    }
  };

  toggleModal = () => {
    this.setState({ isModalVisible: true });
  };

  render() {
    if (this.remainingFunding >= this.minPayment) {
      return (
        <View>
          <Card>
            <Card.Title>
              <Text style={{ fontSize: 20 }}>Funds available!</Text>
            </Card.Title>
            <Card.Title>
              <NumberFormat
                value={this.remainingFunding}
                displayType={"text"}
                thousandSeparator={true}
                prefix={"$"}
                renderText={(value) => (
                  <Text style={{ fontSize: 30 }}>{value}</Text>
                )}
              />
            </Card.Title>
            <Card.Divider />
            <View>
              <View style={{ padding: 5 }}>
                <CurrencyInput
                  style={AquinaLayout.primaryInput}
                  value={this.state.InputFundAmount}
                  onChangeValue={(text) => {
                    this.setState({
                      InputFundAmount: text,
                    });
                  }}
                  unit="$"
                  delimiter=","
                  separator="."
                  precision={2}
                />
              </View>
              <View style={{ padding: 13 }}>
                <TouchableOpacity
                  style={AquinaLayout.primaryButton}
                  disabled={this.state.isLoading}
                  onPress={() => {
                    if (this.state.InputFundAmount) {
                      this.toggleModal();
                    }
                    //this._MakePayment();
                  }}
                >
                  {!this.state.isLoading ? (
                    <Text>Request Funding</Text>
                  ) : (
                    <ActivityIndicator color={"black"} />
                  )}
                </TouchableOpacity>
              </View>
              {this.state.isSuccess || this.state.isError ? (
                <View style={{ padding: 10, alignItems: "center" }}>
                  <Text
                    style={
                      this.state.isError ? { color: "red" } : { color: "green" }
                    }
                  >
                    {this.state.Message}
                  </Text>
                </View>
              ) : null}
            </View>
          </Card>
          <View>
            <Modal isVisible={this.state.isModalVisible}>
              <ConfirmModalContent
                cancelCallback={this._CancelFunding}
                confirmCallback={this._RequestFunding}
                header="Confirm Request"
                content={`By clicking confirm you acknowledge you are requesting ${this.state.InputFundAmount}.`}
              />
            </Modal>
          </View>
        </View>
      );
    } else {
      <View>
        <Card>
          <Card.Title>No funds available at this time!</Card.Title>
        </Card>
      </View>;
    }
  }
}
export default FundingCard;
