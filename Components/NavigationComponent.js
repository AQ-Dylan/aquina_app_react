import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import AquinaLayout from "../Config/style";
//import StackNavigation from "./StackNavigationComponent";

import SettingsScreen from "../Views/SettingsScreen";
import HomeScreen from "../Views/HomeScreen";

const Tab = createBottomTabNavigator();

class NavigationComponent extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Tab.Navigator
        initialRouteName="Dashboard"
        tabBarOptions={{ activeTintColor: AquinaLayout.primaryColor }}
      >
        <Tab.Screen
          name="Dashboard"
          component={HomeScreen}
          options={{
            tabBarLabel: "Dashboard",
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="home" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen
          name="Settings"
          component={SettingsScreen}
          options={{
            tabBarLabel: "Setteings",
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="cog" color={color} size={size} />
            ),
          }}
        />
      </Tab.Navigator>
    );
  }
}

export default NavigationComponent;
