import React from "react";

import { createStackNavigator } from "@react-navigation/stack";

import HomeScreen from "./HomeScreen";

const Stack = createStackNavigator();

export default class StackTest extends React.Component {
  render() {
    return (
      <Stack.Navigator>
        <Stack.Screen name="Dashboard" component={HomeScreen}></Stack.Screen>
      </Stack.Navigator>
    );
  }
}
