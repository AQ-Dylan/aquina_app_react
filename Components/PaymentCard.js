import React from "react";
import {
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator,
  TextInput,
} from "react-native";
import Modal from "react-native-modal";
import { Card } from "react-native-elements";
import AquinaLayout from "../Config/style";
import NumberFormat from "react-number-format";
import ConfirmModalContent from "./ConfirmModalContent";
import CurrencyInput from "react-native-currency-input";

class PaymentCard extends React.Component {
  constructor(props) {
    super(props);
    //console.log(props);
    this.state = {
      isLoading: false,
      PaymentAmount: null,
      isModalVisible: false,
      isError: false,
      isSuccess: false,
      Message: null,
    };
    if (this.props.UnderwrittenAmount * 0.1 <= 5000) {
      this.minPayment = parseFloat(
        (this.props.UnderwrittenAmount * 0.1).toFixed(2)
      );
    } else {
      this.minPayment = 5000;
    }
    //console.log(this.minPayment);
    this.payOffIndicator =
      (100 -
        (parseFloat(this.props.PayOffAmount) /
          parseFloat(this.props.UnderwrittenAmount)) *
          100) /
      100;
    //console.log(this.payOffIndicator);
  }

  _CancelPayment = async () => {
    this.setState({ isModalVisible: false, PaymentAmount: null });
  };

  _MakePayment = async () => {
    this.setState({ isLoading: true });

    var payment = {
      PaymentAmount: this.state.PaymentAmount
        ? parseFloat(
            this.state.PaymentAmount.replace("$", "")
              .replace(" ", "")
              .replace(",", "")
          )
        : 0,
      PaymentType: "PraxisLinePayment",
      PayOffAmount: this.props.PayOffAmount,
    };
    console.log(payment);
    // if (payment.PaymentAmount < this.minPayment) {
    //   this.setState({
    //     isLoading: false,
    //     isModalVisible: false,
    //     isError: true,
    //     isSuccess: false,
    //     Message: `Minimum Payment is ${this.minPayment}`,
    //   });
    // } else {
    // if (
    //   this.state.PaymentAmount > 0 &&
    //   this.state.PaymentAmount <= this.props.PayOffAmount
    // ) {
    //   try {
    //     let data = await aquinaApi._ProcessPaymant(
    //       this.props.OrganizationId,
    //       this.props.Token,
    //       payment
    //     );
    //   } catch (e) {
    //     console.log(e);
    //     alert("Failed to schedule your payment, please try again later.");
    //   }
    // }
    this.setState({
      isModalVisible: false,
      isError: false,
      isSuccess: true,
      Message: "Payment was successful.",
    });
    setTimeout(() => {
      this.setState({
        isLoading: false,
        isError: false,
        isSuccess: false,
        Message: null,
      });
      this.props.AvailableFundsCallback();
    }, 3000);
  };

  toggleModal = () => {
    this.setState({ isModalVisible: true });
  };

  render() {
    return (
      <View>
        <Card>
          <Card.Title>
            <Text style={{ fontSize: 20 }}>Remaining Balance</Text>
          </Card.Title>
          <Card.Title>
            <NumberFormat
              value={this.props.PayOffAmount}
              displayType={"text"}
              thousandSeparator={true}
              prefix={"$"}
              renderText={(value) => (
                <Text style={{ fontSize: 30 }}>{value}</Text>
              )}
            />
          </Card.Title>
          <Card.Divider />
          <View>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-evenly",
                padding: 5,
              }}
            >
              <Text style={{ fontSize: 12 }}>
                Today's payoff, you will save
              </Text>
              <NumberFormat
                value={this.props.PayOffSavingAmount}
                displayType={"text"}
                thousandSeparator={true}
                prefix={"$"}
                renderText={(value) => (
                  <Text style={{ fontSize: 16, fontWeight: "bold" }}>
                    {value}
                  </Text>
                )}
              />
            </View>
            <View style={{ padding: 5 }}>
              <CurrencyInput
                style={AquinaLayout.primaryInput}
                value={this.state.PaymentAmount}
                onChangeValue={(text) => {
                  this.setState({
                    PaymentAmount: text,
                  });
                }}
                unit="$"
                delimiter=","
                separator="."
                precision={2}
              />
            </View>
            <View style={{ padding: 13 }}>
              <TouchableOpacity
                style={AquinaLayout.primaryButton}
                disabled={this.state.isLoading}
                onPress={() => {
                  if (this.state.PaymentAmount) {
                    this.toggleModal();
                  }
                  //this._MakePayment();
                }}
              >
                {!this.state.isLoading ? (
                  <Text>Make Payment</Text>
                ) : (
                  <ActivityIndicator color={"black"} />
                )}
              </TouchableOpacity>
            </View>
            {this.state.isSuccess || this.state.isError ? (
              <View style={{ padding: 10, alignItems: "center" }}>
                <Text
                  style={
                    this.state.isError ? { color: "red" } : { color: "green" }
                  }
                >
                  {this.state.Message}
                </Text>
              </View>
            ) : null}
          </View>
        </Card>
        <View>
          <Modal isVisible={this.state.isModalVisible}>
            <ConfirmModalContent
              cancelCallback={this._CancelPayment}
              confirmCallback={this._MakePayment}
              header="Confirm Payment"
              content={`By clicking confirm you acknowledge that a single payment of ${this.state.PaymentAmount} will be scheduled to withdraw from your account.`}
            />
          </Modal>
        </View>
      </View>
    );
  }
}

export default PaymentCard;
